""" Parsing and processing of flags.
"""
import copy
import re
from typing import List, Callable

from src.dict_utils import get_nested_value, set_nested_value, iterate_over_immutable_values
from src.string_utils import substitute_string
from src.yaml_spec import YamlFormat, PackageManager


def process_flags(yfmt: YamlFormat, flags: dict) -> dict:
    """ Replace all flag variables with values (recursive definitions).

    For example:
    -------------
    Replace
      matches = ['eb.configure_flags_min', 'eb.configure_flags_blas']
    in
      string = '{eb.configure_flags_min} {eb.configure_flags_blas}'
    using the values of `flags[eb][configure_flags_min]` and
    `flags[eb][configure_flags_blas]`, respectively.

    Requirements:
    -------------
    The expected tree structure for flags in the builders.yaml is:
    ```yaml
    flags:
      eb:
        configure_flags_min: '--with-libxc-prefix=$PATH'
        configure_flags_omp: '--enable-openmp'
        ...
      foss:
        cflags_debug: '-g -Wall -O2 -march=native -Wextra -pedantic'
        cflags:       '-Wall -O2 -march=native'
        ...
    ```

    :param yfmt: Yaml parameters.
    :param flags: Dict of flags, with nesting structure consistent with requirements.
    :return: Dict of explicitly-defined flags (variables replaced with values).
    """
    for f in list(flags):
        for name, flag_str in flags[f].items():
            flags[f][name] = process_flag(yfmt, flags, flag_str)
    return flags


def process_flag(yfmt: YamlFormat, flags: dict, flag_str: str) -> str:
    """ Replace variables in flag string.

    Valid Inputs
    --------------
    flag_str = '-O2 -fpe0'                     # Do nothing
    flag_str = '{foss.configure_flags_serial}' # Replace {VAR} using existing entry in flags
    flag_str = '{VAR1} {VAR2} -O2 -fpe0'       # Replace {VAR1} and {VAR2}, preserving the rest

    :param yfmt: Yaml parameters.
    :param flags: Dict of flags. Required to search for substitution definitions.
    :param flag_str: Flag string in which to substitute variables.
    :return: new_flag_str: Flag string with variables subbed for values.
    """
    matches = re.findall(yfmt.regex, flag_str)
    if matches is []:
        return flag_str

    # Want to sub the match string plus curly braces
    targets = [yfmt.variable(m) for m in matches]
    # Retrieve replacements from elsewhere in the flags dict
    replacements = [get_nested_value(flags, keys=yfmt.keys(m)) for m in matches]
    new_flag_str = substitute_string(targets, replacements, flag_str)

    return new_flag_str


def check_flag_substitutions(flags: dict) -> dict:
    """ Check all flags substitutions have been performed.

    :param flags: Dict of flags.
    :return: missed_flags: Flags with variables still included.
    If none exist, an empty dict is returned.
    """
    yfmt = YamlFormat()
    missed_flags = {}

    # Return to dict, declared in caller's scope
    def return_to_missed_flag(key, value):
        matches = re.findall(yfmt.regex, value)
        if matches:
            k = "".join(s + yfmt.delimiter for s in key)[:-1]
            missed_flags[k] = value

    # Recursively iterate over nested tree structure
    iterate_over_immutable_values(flags, return_to_missed_flag)

    return missed_flags


def get_lib_root_replacements(flags: dict, yfmt: YamlFormat, package_manager: PackageManager) -> dict:
    """ Find flag strings containing generic library prefix and return the
    substituted string values.

    Routine Description
    -------------------------
    Match generic library variables: '$PMROOT_LIBXC'

    re.findall returns: matches = ['PMROOT_LIBXC']

    Replace 'PMROOT_LIBXC' with 'EBROOTLIBXC' or 'MPSD_LIBXC_ROOT'
    using `replacement_func(['PMROOT_LIBXC'])`

    Do any replacements in a full flag string and return in a dict
    where the keys are concatenated i.e. 'pm.configure_flags_min'
    corresponds to ['pm']['configure_flags_min']

    :param flags: Flags dictionary.
    :param yfmt: Yaml input specifications
    :param package_manager: Package manager
    :return: lib_replacements: Dict of replacements, with concatenated keys
    """
    lib_replacements = {}
    replacement_func: Callable = yfmt.libvar_name_setter(package_manager)

    def find_matches(keys: List[str], value: str):
        matches = re.findall(yfmt.library_var_regex, value)
        if matches:
            replacements = replacement_func(matches)
            new_value = substitute_string(matches, replacements, value)
            full_key = "".join(k + yfmt.delimiter for k in keys)[:-1]
            lib_replacements[full_key] = new_value

    iterate_over_immutable_values(flags, find_matches)

    return lib_replacements


def substitute_lib_root_variable(package_manager: PackageManager, flags: dict) -> dict:
    """ Substitute the library root variable in flags.

    Recursively iterate over the whole flag dict.
    When a generic library variable, such as $PMROOT_LIBXC is matched, replace it with the specific
    package manager prefix, such as $EBROOTLIBXC.

    Note, the top level key of `flags`, 'pm', is not substituted.

    :param package_manager: Package manager string.
    :param flags: Flags dictionary.
    :return updated_flags: Copy of flags dictionary, with lib_prefix replaced with the
    prefix of the package manager.
    """
    yfmt = YamlFormat()
    replacements = get_lib_root_replacements(flags, yfmt, package_manager)

    updated_flags = copy.deepcopy(flags)
    for full_key, replacement in replacements.items():
        keys = yfmt.keys(full_key)
        updated_flags = set_nested_value(updated_flags, keys, replacement)

    return updated_flags
