""" Builder configurations class.
"""
import copy
from copy import deepcopy
from pathlib import Path
from typing import Dict, Union, Optional
import yaml

from src.yaml_spec import YamlFormat, PackageManager
from src.parse_flags import process_flags, process_flag, check_flag_substitutions, substitute_lib_root_variable


class BuilderConfig:
    """ Class for parsing and processing the builders.yaml config file.

    Example Usage
    --------------
    ```
       config = BuilderConfig(file_name="path/to/builders.yaml")
       eb_builders: dict = config.get_builders(PackageManager.EB)

       assert 'foss' in config.valid_toolchains
       for version, builder in eb_builders['foss'].items():
           c['builders'].append(octopus_autotools_builder(**builder))
    ```
    """
    def __init__(self, file_name: Optional[Union[str, Path]] = None, perform_checks=False):
        """ Parse YAML builders configuration file.

        :param file_name: YAML builders file name.
        :param perform_checks: Perform checks on flags and builders.
        """
        self.yfmt = YamlFormat()

        self.file_name = Path(self.yfmt.file_name) if file_name is None else Path(file_name)

        self.perform_checks = perform_checks

        # All yaml file contents
        self._config = self._parse_yaml()

        # Environment variables
        self.env_vars = deepcopy(self._config['environment_variables'])

        # Builder optional defaults
        self._opt_builder_defaults = deepcopy(self._config['optional_builder_defaults'])
        if self._opt_builder_defaults is None:
            raise ValueError(f'{self.file_name} missing required entry "optional_builder_defaults"')

        if self.perform_checks:
            all_fields = set(self._config.keys())
            unused_fields = all_fields - self.yfmt.valid_toplevel_keys
            raise ValueError(f'Unrecognised top-level keys present in {self.file_name}: {unused_fields}\n'
                             f'Valid keys specified in YamlFormat()')

    def get_builders(self, package_manager: PackageManager) -> dict:
        """ Get builders for a given package manager.

        All generic flags are substituted with package manager-specific variables.
        All recursive flag definitions are explicitly defined.
        Any fields not explicitly specified in the yaml are assigned with defaults.

        :param package_manager: Package manager.
        :return: updated_builders: Builders, with all fields and flags filled in.
        """
        flags = self.set_flags(package_manager)
        builders = self._initialise_builders(package_manager)
        updated_builders = self._substitute_builder_flags(builders, flags)
        return updated_builders

    def set_flags(self, package_manager: PackageManager) -> dict:
        """ Set flags from YAML file input.

        Substitute for library root variable.
        Substitute for flag variables (flags defined in terms of other flags).

        :param package_manager: Package manager
        :return: Flags with all variables substituted for values.
        """
        flags = deepcopy(self._config['flags'])
        flags = substitute_lib_root_variable(package_manager, flags)
        flags = process_flags(self.yfmt, flags)

        if self.perform_checks:
            missed_flags = check_flag_substitutions(flags)
            if missed_flags:
                raise ValueError(f"Some flags still contain variables: {missed_flags}")

        return flags

    def valid_toolchains(self):
        return self.yfmt.valid_toolchains

    def _parse_yaml(self) -> dict:
        """ Parse YAML builders file.

        :return: config: File contents.
        """
        if not self.file_name.is_file():
            raise FileNotFoundError(f'File not found: {self.file_name}')

        with open(self.file_name, "r") as fid:
            try:
                config = yaml.safe_load(fid)
            except yaml.YAMLError:
                raise yaml.YAMLError(f'Invalid formatting in YAML file: {self.file_name}')
        return config

    @staticmethod
    def builder_name(tool_chain: str, version: str) -> str:
        return tool_chain + '-' + version

    def _initialise_builders(self, package_manager: PackageManager) -> dict:
        """ Initialise builders.

        Requires a specific nested structure for the builders,
        defined in the YAML file.

        # Input builders, for a given package manager.
        input_builders = \
          {'foss':                   # First toolchain
                {'2020a_mpi':        # First toolchain builder (labelled by version)
                  {
                   'workers': 'tentacles',
                   'var':
                       {
                           'CFLAGS': '{foss.cflags}',
                           'CXXFLAGS': '{foss.cxxflags}',
                           'FCFLAGS': '{foss.fflags}',
                           'LOCAL_MAKEFLAGS': '-j 16',
                           'OCT_TEST_NJOBS': 16
                       },
                   'flags': '{foss.configure_flags_serial}'
                   }
              },
              {'2020b_mpi':    # Second toolchain builder
                  {
                    ...
                   }
              }
          'intel':    # Second tool chain
           ...
         }

        :return: Tuple[builders, missing_toolchains]: Builders (for the associated package manager) with any fields not
        specified in input, initialised from defaults. Any toolchains that are valid but were not present in the YAML
        config.
        """
        # Builders specified in config file, for a given package manager
        input_builders = copy.deepcopy(self._config[package_manager.name])

        # Initialise
        builders: Dict[str, dict] = {key: dict() for key in input_builders.keys()}

        # Assumes nesting in function documentation
        for tool_chain, toolchain_builders in input_builders.items():
            for version, fields in toolchain_builders.items():
                more_fields = {'buildname': self.builder_name(tool_chain, version),
                               'toolchain': tool_chain,
                               'version': version,
                               'package_manager': package_manager}
                builder = initialise_builder(fields, self._opt_builder_defaults, **more_fields)
                builders[tool_chain].update({version: builder})

        return builders

    def _substitute_builder_flags(self, builders: dict, flags: dict) -> dict:
        """ Substitute builder flag variables for definitions.

        Iterate through the flag fields of every builder, and replace
        variable definitions used in 'flags' and 'var'.

        Note, it was most straightforward to hard-code the required fields
        to update, i.e. 'flags' and 'var', rather than try and implement
        generically.

        :return: updated_builders. Builders with all flags substituted.
        """
        updated_builders = copy.deepcopy(builders)
        for tool_chain, toolchain_builders in builders.items():
            for version, fields in toolchain_builders.items():
                fields['flags'] = self._substitute_flag(flags, fields['flags'])
                fields['var']['CFLAGS'] = self._substitute_flag(flags, fields['var']['CFLAGS'])
                fields['var']['CXXFLAGS'] = self._substitute_flag(flags, fields['var']['CXXFLAGS'])
                fields['var']['FCFLAGS'] = self._substitute_flag(flags, fields['var']['FCFLAGS'])
                updated_builders[tool_chain][version] = fields
        return updated_builders

    def _substitute_flag(self, flags: dict, flag_str: str) -> str:
        """ Substitute flag variable/s for definition/s.

        :param flags: Dict of flags. Required to search for substitution definitions.
        :param flag_str: Flag string, possibly containing variables.
        :return: subbed_flag: Flag string with any variable substituted for definition.
        """
        return process_flag(self.yfmt, flags, flag_str)


def initialise_builder(input_builder: dict, optional_defaults: dict, **kwargs) -> dict:
    """ Initialise a builder.

    Expect `input_builder` to contain at least mandatory fields:
    ```
     input_builder = {
                'workers': 'tentacles',
                'var': {
                        'CFLAGS': '{foss.cflags}',
                        'CXXFLAGS': '{foss.cxxflags}',
                        'FCFLAGS': '{foss.fflags}',
                        'LOCAL_MAKEFLAGS': '-j 16',
                        'OCT_TEST_NJOBS': 16
                        },
                'flags': '{foss.configure_flags_serial}'
                }

    ```
    `input_builder` can also contain any optional flags, which will be
    preserved over the defaults.

    :param input_builder: Builders dict parsed from the YAML file.
    :param optional_defaults: Default values for optional build options. Used if not
    specified in the config file.
    :param kwargs: Should contain key:values which are determined from higher-level dict
    keys (buildname, for example).
    :return: builder: Builder with all fields defined.
    """
    builder = {**kwargs, **optional_defaults}
    # Where the default fields differ from the parsed input values (or are not present), overwrite
    builder.update(**input_builder)
    return builder
