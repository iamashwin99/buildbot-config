""" Yaml specifications
"""
import enum
from dataclasses import dataclass
from typing import List, Callable


class PackageManager(enum.Enum):
    """Valid package managers"""
    EB = enum.auto()
    SPACK = enum.auto()

    @classmethod
    def names(cls) -> List[str]:
        """Get a list of names
        :return List of names
        """
        return [pm.name for pm in cls]


@dataclass
class YamlFormat:
    """ Define the specific notation used in the builders YAML
    file to define variables.
    """
    # File name
    file_name = "builders.yaml"

    # Regex matches between braces, {VAR}, using \{(.*?)\
    # but will ignore ${VAR}, using (?<!\$)
    regex = r'(?<!\$)\{(.*?)\}'

    # Delimiter used in flag variable definition
    delimiter: str = '.'

    # Generic library prefix
    generic_lib_prefix = 'PMROOT'

    # regex to match library variable names
    # i.e. match $PMROOT_LIBXC, return PMROOT_LIBXC
    library_var_regex = rf"\$({generic_lib_prefix}_[^\s\W]+)"

    # Valid toolchains
    valid_toolchains = {'foss', 'intel'}

    # Valid top-level keys
    valid_toplevel_keys = {'environment_variables', 'flags', 'optional_builder_defaults', 'EB', 'SPACK'}

    @staticmethod
    def variable(x: str) -> str:
        """Flag variable definition in YAML.
        """
        return '{' + x + '}'

    def keys(self, key_string: str) -> List[str]:
        """ Create a list of keys from a concatenated string.
        i.e. Input 'a.b.c.' and return ['a', 'b', 'c']
        """
        return key_string.split(self.delimiter)

    def libvar_name_setter(self, package_manager: PackageManager) -> Callable[[List[str]], List[str]]:
        """ Return a function defining library variables, for a given package manager.

        The library variable naming convention differs between package managers. For example:
         Package manager       Variable
            EASYBUILD         EBROOTLIBXC
            SPACK             MPSD_LIBXC_ROOT

        :param package_manager: Package manager
        :return Function that accepts the generic library variable str and returns
        the package manager-specific library variable str.
        """
        lib_var_name = {PackageManager.EB: self.set_eb_library_variable_name,
                        PackageManager.SPACK: self.set_spack_library_variable_name}
        try:
            return lib_var_name[package_manager]
        except KeyError:
            raise KeyError(f'Package manager not recognised: {package_manager.name}\n'
                             f'Valid choices are {PackageManager.names()}.')

    def extract_lib(self, lib_var: str) -> str:
        """ Extract library name from library variable.

        Convention of generic library variables is:
        'PMROOT_LIBNAME'
        Return 'LIBNAME'

        Note, a simple implementation like `lib_var.split('_')[-1]`
        will fail for a variable like 'PMROOT_ETSF_IO'
        """
        prefix = self.generic_lib_prefix + '_'
        try:
            i_start = lib_var.index(prefix)
        except ValueError:
            raise ValueError(f'"{prefix}" not in library variable {lib_var}')

        i = i_start + len(prefix)
        return lib_var[i:]

    def set_eb_library_variable_name(self, lib_variables: List[str]) -> List[str]:
        return ['EBROOT' + self.extract_lib(var) for var in lib_variables]

    def set_spack_library_variable_name(self, lib_variables: List[str]) -> List[str]:
        return ['MPSD_' + self.extract_lib(var) + '_ROOT' for var in lib_variables]
