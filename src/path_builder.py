""" Functions that comprise Octopus VPath Builder
"""
from copy import deepcopy
from typing import Tuple, Union


def initialise_env(var: dict) -> dict:
    """ Initialise env with var.

    TODO(Alex) Issue 34. Move env defaults to builders.yaml, then remove this function
    Which would reduce this to myenv = deepcopy(var)

    :param var: Builder environment variables.
    :return: myenv: Environment dictionary with defaults + var key,values.
    """
    myenv = {"LOCAL_MAKEFLAGS": "-j", "OMP_NUM_THREADS": "1", "LC_ALL": "C.UTF-8"}
    for key, value in var.items():
        myenv[key] = value
    return myenv


def add_code_coverage_to_env(myenv: dict) -> dict:
    """ Add code coverage to environment dictionary.

    :param myenv: Environment dictionary
    :return: env: Environment dictionary with code cov flags added.
    """
    env = deepcopy(myenv)
    for i in ["CFLAGS", "CXXFLAGS", "FCFLAGS"]:
        if i in myenv:
            env[i] = env[i] + ' -ftest-coverage -fprofile-arcs'
        else:
            env[i] = '-ftest-coverage -fprofile-arcs'
    return env


def set_toolchain_variant(variant: str, cuda: bool, mpi: bool, valgrind: bool) -> str:
    """ Set toolchain variant string.

    :param variant: Variant type.
    :param mpi: Is MPI.
    :param cuda: Is CUDA.
    :param valgrind: Use valgrind
    :return: Toolchain variant string.
    """
    d_cuda = {True: '-CUDA', False: ''}
    d_mpi = {True: '-mpi', False: ''}
    d_valgrind = {True: " Valgrind", False: ''}
    return variant + d_cuda[cuda] + d_mpi[mpi] + d_valgrind[valgrind]


def eb_module_command(modulecmd: str, toolchain: str, version: str, toolchain_variant: str) -> str:
    """ Define EasyBuild module command.

    :param modulecmd: Module command prefix (path/to/Lua, for example)
    :param toolchain: Toolchain
    :param version: Toolchain version
    :param toolchain_variant: Toolchain variant
    :return: modules_command: Full module command, appended with `&&`, implying `modules_command`
    MUST be appended in a subsequent call.
    """
    modules_command = "eval `" + modulecmd + " sh use $EASYBUILD_PREFIX/" + version + "/modules/Core ` && "
    modules_command = modules_command + "eval `" + modulecmd + " sh load " + toolchain + " Octopus/" + toolchain_variant
    # This assumes additional string appends follow
    modules_command = modules_command + " ` && "
    return modules_command


def export_env_flags(modules_command: str, myenv: dict) -> Tuple[str, dict]:
    """ Export specific environment flags.
    The following flags need to be exported _after_ loading the modules

    :param modulescommand: Module commabd.
    :param myenv: Environment variables.
    :return: Tuple of updated modules command and environment variables
    """
    cmd = modules_command
    env = deepcopy(myenv)
    for flag in ["CC", "CXX", "FC", "CFLAGS", "CXXFLAGS", "FCFLAGS", "FCFLAGS_ELPA", "FCFLAGS_FFTW", "MPIEXEC"]:
        if flag in myenv:
            cmd += f'export {flag}="{myenv[flag]}" && '
            env.pop(flag)
    return cmd, env


def set_test_command(test_command: Union[str, None], test: str) -> str:
    """ Set Octopus test command

    :param test_command: Builder-specific test command
    :param test: Test type.
    :return: test_command: Test command string
    """
    if test_command is not None:
        return test_command

    check = {'short': "check-short", 'long': "check-long", 'full': 'check'}
    if test not in list(check):
        raise ValueError(f'`test` value, {test}, is not valid for running the Octopus test suite.\n'
                         f'Valid choices are {list(check)}')

    test_command ="ulimit -s unlimited; ulimit -t 7200; cd _build && CHECK_REPORT=%(prop:buildername)s_" \
                  "%(src::revision)s make " + check[test]

    return test_command
