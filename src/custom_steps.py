""" Custom Build Steps

Note, if one inherits from BuildStep, the base init will complain
- One needs to be more specific w.r.t. base classes.

from buildbot.process.buildstep import BuildStep
"""
from abc import ABC
from typing import Union, Optional

from buildbot.process.results import SUCCESS
from buildbot.plugins import util, steps
from buildbot.steps.shell import ShellCommand
from buildbot.steps.source.gitlab import Git, GitLab
from buildbot.process.results import SKIPPED

from src.builder import PackageManager
from src.url_builder import url_factory


# Instance of URLs
_url = url_factory()


GitLabStep = GitLab(repourl=util.Interpolate('%(prop:repository)s'),
                    mode='full',
                    submodules=False,
                    retry=(30, 20),
                    retryFetch=True)

BBGitStep = Git(repourl=_url.bbscripts_git(),
                mode='full',
                submodules=False,
                retry=(30, 20),
                retryFetch=True,
                alwaysUseLatest=True,
                hideStepIf=lambda results, s: results == SUCCESS,
                workdir='buildbot-scripts')

AutotoolsStep = ShellCommand(description="generating configure",
                             descriptionDone="generate configure",
                             command=["autoreconf", "-i"],
                             haltOnFailure=True)

MakevarsStep = ShellCommand(description='generating varinfo',
                            descriptionDone='make varinfo',
                            command='./build/mk_varinfo.pl && ./build/var2html.pl',
                            haltOnFailure=False)

DoxygenStep = ShellCommand(description='generating doxygen doc',
                           descriptionDone='generate doxygen doc',
                           command='cd doc/doxygen && ./create_documentation.sh > doxy.out 2> doxy.err',
                           timeout=1800,
                           logfiles={'doxy.out': 'doc/doxygen/doxy.out',
                                     'doxy.err': 'doc/doxygen/doxy.err'},
                           haltOnFailure=False)

FindentStep = ShellCommand(description='run findent',
                           descriptionDone='findent finished',
                           command='findent-octopus_batch --dir=./src --dry-run',
                           timeout=1800,
                           haltOnFailure=True,
                           logfiles={'findent.patch': 'findent.patch',
                                     'stdout': 'stdout'})

class OctopusTestingStep(ShellCommand, ABC):
    """ Octopus Test Builder.

    Example Usage:

    ```
    builder.addStep(OctopusTestStep(modules_command=modulescommand,
                                    test=test,
                                    test_command=testCommand,
                                    timeout=timeout,
                                    doStepIf=run_tests))
    ```
    """
    # Valid `test` values
    _check = {'short': "check-short", 'long': "check-long", 'full': 'check'}
    
    def __init__(self,
                 modules_command: str,
                 test: str,
                 test_command: Optional[Union[str, None]] = None,
                 **kwargs):

        if test not in list(self._check):
            raise ValueError(f'`test` value, {test}, is not valid for running the Octopus test suite.\n'
                             f'Valid choices are {list(self._check)}')

        self.modules_command = modules_command
        self.test = test
        self.test_command = self.set_test_command(test_command)

        kwargs['description'] = 'testing'
        kwargs['descriptionDone'] = 'test'
        kwargs['command'] = util.Interpolate(self.modules_command + self.test_command)
        kwargs['env'] = kwargs.get('env', {})
        kwargs['hideStepIf'] = lambda results, s: results == SKIPPED

        super().__init__(**kwargs)

    def set_test_command(self, test_command: Union[str, None]) -> str:
        """ Set Octopus test command.
        """
        if test_command:
            return test_command

        cmd = "ulimit -s unlimited; ulimit -t 7200; cd _build && CHECK_REPORT=%(prop:buildername)s_" \
              "%(src::revision)s make " + self._check[self.test]

        return cmd


class ValgrindTestingStep(OctopusTestingStep):
    """ Octopus testing step run through Valgrind.
    """

    _valgrind_env = {"EXEC": util.Interpolate(
        "valgrind --track-origins=yes --leak-check=full --show-leak-kinds=all "
        "--errors-for-leak-kinds=all --gen-suppressions=all --error-exitcode=1 "
        "--suppressions=%(prop:builddir)s/buildbot-scripts/valgrind/%(prop:buildername)s.supp")
    }

    def __init__(self,
                 modules_command: str,
                 test: str,
                 package_manager: PackageManager,
                 test_command: Optional[Union[str, None]] = None,
                 **kwargs):
        self.modules_command = modules_command + self.valgrind_command(package_manager)
        self.test = test
        self.package_manager = package_manager
        self.test_command = test_command

        kwargs['env'] = self._valgrind_env

        super().__init__(self.modules_command, self.test, self.test_command, **kwargs)

    @staticmethod
    def valgrind_command(pm: PackageManager) -> str:
        """ Valgrind command string.
        """
        if pm is not PackageManager.EB:
            raise ValueError('Valgrind command only implemented for EB')
        return 'export LD_PRELOAD="$EBROOTVALGRIND/lib/valgrind/libmpiwrap-amd64-linux.so" && '


class OctopusCodeCovUploadStep(steps.ShellSequence, ABC):
    """ Octopus Test Builder, with code coverage.

    Example usage:
    ```
    builder.addStep(OctopusCodeCovUploadStep(modules_command=modulescommand
                                              workdir="build/_build/src"
                                              doStepIf=run_tests)
                    )
    ```

    ShellSequence differs from ShellCommand in that it accepts multiple commands.
    See [here](https://docs.buildbot.net/latest/manual/configuration/steps/shell_sequence.html)
    """
    _default_workdir = "build/_build/src"

    # Code coverage shell arguments
    download = util.ShellArg(
        command=["curl", "--connect-timeout", "5", "--max-time", "10",
                 "--retry", "5", "--retry-delay", "0", "-s",
                 "https://codecov.io/bash", "-o", "codecov.sh"],
        logname='download'
    )

    cleanup = util.ShellArg(command=["rm", "codecov.sh"], logname='cleanup')

    # Environment
    exec_env = {'CI_BUILD_URL': util.Interpolate('%(kw:url)s', url=util.URLForBuild),
                'CI_BUILD_ID': util.Interpolate('%(prop:buildername)s/%(prop:buildnumber)s')
                }

    def __init__(self,
                 modules_command: str,
                 workdir=None,
                 **kwargs):
        self.modules_command = modules_command
        self.workdir = workdir if workdir else self._default_workdir

        kwargs['description'] = 'uploading code coverage'
        kwargs['descriptionDone'] = 'code coverage'
        kwargs['workdir'] = self.workdir
        kwargs['commands'] = [self.download, self.upload(), self.cleanup]
        kwargs['env'] = self.exec_env
        kwargs['hideStepIf'] = lambda results, s: results == SKIPPED

        super().__init__(**kwargs)

    def upload(self):
        return util.ShellArg(
            command=util.Interpolate(
                self.modules_command + "bash codecov.sh -t %(secret:codecov_token)s"),
            logname='upload')


class OctopusTestPerformanceStep(ShellCommand, ABC):
    _command = "ulimit -s unlimited; cd _build && make check-performance"

    def __init__(self,
                 module_command: str,
                 **kwargs):
        self.module_command = module_command
        self.command = util.Interpolate(self.module_command + self._command)

        kwargs['description'] = 'testing performance'
        kwargs['descriptionDone'] = 'performance test'
        kwargs['command'] = self.command
        kwargs['hideStepIf'] = lambda results, s: results == SKIPPED

        super().__init__(**kwargs)


class OctopusUploadStep(ShellCommand, ABC):
    def __init__(self,
                 step: str,
                 results_file: str,
                 upload_url: str,
                 flunkOnFailure=False,
                 haltOnFailure=False,
                 **kwargs):

        self.step = step
        self.results_file = results_file
        self.upload_url = upload_url
        self.flunkOnFailure = flunkOnFailure
        self.haltOnFailure = haltOnFailure
        self.command = util.Interpolate(f'curl -F "file={self.results_file}" '
                                        f'-F builder_name=%(prop:buildername)s -F commit_hash=%(src::revision)s '
                                        f'-F token=\'%(secret:testsuite_token)s\' {self.upload_url}'
                                        )

        kwargs['description'] = f"uploading {self.step} results"
        kwargs['descriptionDone'] = f"upload {self.step} results"
        kwargs['command'] = self.command
        kwargs['flunkOnFailure'] = self.flunkOnFailure
        kwargs['haltOnFailure'] = self.haltOnFailure
        kwargs['hideStepIf'] = lambda results, s: results == SKIPPED

        super().__init__(**kwargs)


class OctopusPerformanceUploadStep(OctopusUploadStep, ABC):
    def __init__(self, **kwargs):

        super().__init__(step='performance',
                         results_file="@_build/testsuite/performance/performancedata.yaml",
                         upload_url=_url.title() + "/testsuite/performance/result/add",
                         **kwargs)


class OctopusTestUploadStep(OctopusUploadStep, ABC):
    def __init__(self, **kwargs):

        super().__init__(step='testsuite',
                         results_file="@_build/testsuite/%(prop:buildername)s_%(src::revision)s.yaml",
                         upload_url=_url.title() + "/testsuite/result/add",
                         **kwargs)
