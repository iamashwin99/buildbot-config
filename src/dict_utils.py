""" Utilities for operating on dictionaries
"""
from collections.abc import Mapping
import copy
from typing import Callable, List


def key_missing_message(keys: List[str], i: int):
    msg = "Key sequence: " + "".join(f'[{k}]' for k in keys[:i + 1]) + \
          " not present in dictionary"
    return msg


def get_nested_value(d: dict, keys: List[str]):
    """ Given a concatenated string of dict keys, retrieve the corresponding
    dictionary value.

    For example, to retrieve the nested value d['a']['b']['c'], call:
    `value = get_nested_value(d, ['a','b','c'])`

    :param d: Nested dictionary.
    :param keys: List of keys.
    :return: Value.
    """
    nested = copy.deepcopy(d)
    for i, key in enumerate(keys):
        try:
            nested = nested[key]
        except KeyError:
            raise KeyError(key_missing_message(keys, i))
    return nested


def set_nested_value(dic: dict, keys: List[str], value) -> dict:
    """ Set value of nested key in a dictionary.

    :param dic: Dictionary.
    :param keys: List of keys.
    :param value: Value to assign to nested key.
    :return: dic: Input dictionary, with nested key value set to `value`.
    """
    d = dic

    # Iterate through nested keys (excluding final key)
    for i, key in enumerate(keys[:-1]):
        try:
            d = d[key]
        except KeyError:
            raise KeyError(key_missing_message(keys, i))

    # Assign value for final key
    if d.get(keys[-1]) is None:
        raise KeyError(key_missing_message(keys, len(keys)+1))

    d[keys[-1]] = value

    return dic


def iterate_over_immutable_values(nested, func: Callable) -> None:
    """ Iterate over all key,value 's of a dictionary and pass immutable values
    (essentially not containers) to `func`.

    :param nested: Input dictionary.
    :param func: Function defining what to do with key,value pairs.
    :return: None: Implicitly returns via the input function `func`.
    """
    def _iterate_over_immutable_values(nested, func: Callable, full_key: List[str]):
        """ Implementation.
        :param full_key: Keeps track of the keychain leading to values.
        """
        for key, value in nested.items():
            full_key += [key]
            if isinstance(value, Mapping):
                _iterate_over_immutable_values(value, func, full_key)
                full_key = full_key[:-1]
            else:
                func(full_key, value)
            full_key = full_key[:-1]

    return _iterate_over_immutable_values(nested, func, full_key=[])
