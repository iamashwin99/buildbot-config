"""String utilities
"""
from typing import List
import re


def substitute_string(matches: List[str], replacements: List[str], string: str) -> str:
    """ Replace matches in a string containing.

    If matches = [], string is returned.
    Matches not present in string are ignored by re.

    :param matches: List of sub-string matches.
    :param replacements: List of sub-string replacements.
    :param string: String contain matches to replace.
    :return: new_string: String with matches replaced.
    """
    if len(matches) != len(replacements):
        raise ValueError('Each match should have a replacement')
    new_string = string
    for i in range(len(matches)):
        pattern = re.compile(matches[i])
        new_string = pattern.sub(replacements[i], new_string)
    return new_string
