import pytest
import pathlib
import yaml

from src.yaml_spec import YamlFormat, PackageManager
from src.builder import BuilderConfig, initialise_builder


class MockFile:
    """ Class for testing parsers that require either:
       * File.
       * File string contents.

    Usage:
    @pytest.fixture
    def file_mock(tmp_path):
       file = tmp_path / _file_name
       file.write_text(string_contents)
       return MockFile(file, string_contents)
    """

    def __init__(self, file: pathlib.Path, string: str):
        # File object
        self.file = file
        # File contents
        self.string = string
        # Name prepended by path
        self.full_path = self.file.as_posix()


@pytest.fixture()
def yfmt() -> YamlFormat:
    return YamlFormat()


@pytest.fixture()
def badly_formatted_yaml(yfmt, tmp_path) -> MockFile:
    file = tmp_path / yfmt.file_name
    builders_yaml_str = """
    foss:
      foss-2020b_mpi:
        workers: 'tentacles   # Not closed the string
    """
    file.write_text(builders_yaml_str)
    return MockFile(file, builders_yaml_str)


def test_parse_yaml_exceptions(tmp_path, badly_formatted_yaml):

    with pytest.raises(FileNotFoundError) as error:
        b_config = BuilderConfig('fake-file-path')
    assert error.value.args[0] == 'File not found: fake-file-path'

    with pytest.raises(yaml.YAMLError) as error:
        b_config = BuilderConfig( badly_formatted_yaml.file)
    assert error.value.args[0] == f'Invalid formatting in YAML file: {badly_formatted_yaml.full_path}'


@pytest.fixture()
def optional_builder_defaults() -> dict:
    defaults = {
       'variant': '',
       'mpi': False,
       'cuda': False,
       'test': 'full',
       'timeout': 1800,
       'testCommand': None,
       'modulecmd': '/usr/share/lmod/lmod/libexec/lmod',
       'codeCoverage': False,
       'valgrind': False,
       'failOnCompilerWarnings': False}
    return defaults


def test_initialise_builder(optional_builder_defaults):
    """ Initialise an input builder specified in YAML containing required fields, only.
    """
    input_builder = {'workers': 'tentacles',
                     'var':
                         {
                             'CFLAGS': '{foss.cflags}',
                             'CXXFLAGS': '{foss.cxxflags}',
                             'FCFLAGS': '{foss.fflags}',
                             'LOCAL_MAKEFLAGS': '-j 16',
                             'OCT_TEST_NJOBS': 16
                         },
                     'flags': '{foss.configure_flags_serial}'
                     }

    name_fields = {'buildname': 'foss-2020b',
                   'toolchain': 'foss',
                   'version': '2020b'
                   }

    output_builder = initialise_builder(input_builder, optional_builder_defaults, **name_fields)

    ref_builder = {'buildname': 'foss-2020b',
                   'toolchain': 'foss',
                   'version': '2020b',
                   'variant': '',
                   'mpi': False,
                   'cuda': False,
                   'test': 'full',
                   'timeout': 1800,
                   'testCommand': None,
                   'modulecmd': '/usr/share/lmod/lmod/libexec/lmod',
                   'codeCoverage': False,
                   'valgrind': False,
                   'failOnCompilerWarnings': False,
                   'workers': 'tentacles',
                   'var':
                       {'CFLAGS': '{foss.cflags}',
                        'CXXFLAGS': '{foss.cxxflags}',
                        'FCFLAGS': '{foss.fflags}',
                        'LOCAL_MAKEFLAGS': '-j 16',
                        'OCT_TEST_NJOBS': 16},
                   'flags': '{foss.configure_flags_serial}'
                   }

    assert output_builder == ref_builder


def test_initialise_builder_with_optionals_specified(optional_builder_defaults):
    """ Initialise an input builder specified in YAML with all required and optional fields
    explicitly defined.
    """
    name_fields = {'buildname': 'foss-2020b-mpi',
                   'toolchain': 'foss',
                   'version': '2020b-mpi'
                   }

    input_builder = {'variant': 'some-variant',
                     'mpi': True,
                     'cuda': True,
                     'test': 'dist',
                     'timeout': 900,
                     'testCommand': 'some-cmd',
                     'modulecmd': '/usr/share/lmod/lmod/libexec/lmod',
                     'codeCoverage': True,
                     'valgrind': True,
                     'failOnCompilerWarnings': True,
                     'workers': 'tentacles',
                     'var':
                         {
                             'CFLAGS': '{foss.cflags}',
                             'CXXFLAGS': '{foss.cxxflags}',
                             'FCFLAGS': '{foss.fflags}',
                             'LOCAL_MAKEFLAGS': '-j 16',
                             'OCT_TEST_NJOBS': 16
                         },
                     'flags': '{foss.configure_flags_serial}'
                     }

    output_builder = initialise_builder(input_builder, optional_builder_defaults, **name_fields)

    # Pop fields that get set from `name_fields`
    for key, expected_value in name_fields.items():
        assert output_builder.pop(key) == expected_value

    assert output_builder == input_builder, 'All fields are specified ' \
                                            '-> Nothing should be overwritten by the defaults'


@pytest.fixture()
def builders_yaml_str() -> str:
    string = """
    environment_variables:
        buildbot_modulecmd: &buildbot_modulecmd '/usr/share/lmod/lmod/libexec/lmod'

    optional_builder_defaults:
       variant: ''
       mpi: False
       cuda: False
       test: 'full'
       timeout: 1800
       testCommand:
       modulecmd: *buildbot_modulecmd
       codeCoverage: False
       valgrind: False
       failOnCompilerWarnings: False

    # Builder flags
    # Order of definitions matters
    flags:
      pm:
        configure_flags_min: '--with-libxc-prefix=$PMROOT_LIBXC --with-gsl-prefix=$PMROOT_GSL'
        configure_flags_common: '--common-flags'
        configure_flags_mpi: '--enable-mpi --with-elpa-prefix=$PMROOT_ELPA'
        configure_flags_blas: '--with-blas=""-L$BLAS_LIB_DIR $LIBBLAS""'
      foss:
        cflags:          '-Wall -O2 -march=native'
        cxxflags:        '-Wall -O2 -march=native'
        fflags:       '-Wall -Wno-maybe-uninitialized -Wno-surprising -O2 -march=native -fbacktrace'
        configure_flags_common: '{pm.configure_flags_mpi} {pm.configure_flags_common} --with-fftw-prefix=$PMROOT_FFTW'
        configure_flags_mpi_common: '{foss.configure_flags_common} {pm.configure_flags_mpi} --with-pfft-prefix=$PMROOT_PFFT'
        configure_flags_serial: '{foss.configure_flags_common} {pm.configure_flags_blas}'

    # Builder
    EB:
      foss:
        2020b_mpi:
          workers: 'tentacles'
          var:
            CFLAGS: '{foss.cflags}'
            CXXFLAGS: '{foss.cxxflags}'
            FCFLAGS: '{foss.fflags}'
            LOCAL_MAKEFLAGS: '-j 16'
            OCT_TEST_NJOBS: 16
          flags: '{foss.configure_flags_serial}'
    """
    return string


@pytest.fixture()
def builder_config(tmp_path, yfmt, builders_yaml_str) -> BuilderConfig:
    file = tmp_path / yfmt.file_name
    file.write_text(builders_yaml_str)
    config = BuilderConfig(file, perform_checks=False)
    return config


def test_builderconfig_env_vars(builder_config):

    assert builder_config.env_vars == {'buildbot_modulecmd': '/usr/share/lmod/lmod/libexec/lmod'}


def test_builderconfig_set_flags(builder_config):

    ref_flags = {'pm': {'configure_flags_min': '--with-libxc-prefix=$EBROOTLIBXC --with-gsl-prefix=$EBROOTGSL',
                        'configure_flags_common': '--common-flags',
                        'configure_flags_mpi': '--enable-mpi --with-elpa-prefix=$EBROOTELPA',
                        'configure_flags_blas': '--with-blas=""-L$BLAS_LIB_DIR $LIBBLAS""'},
                 'foss': {'cflags': '-Wall -O2 -march=native',
                          'cxxflags': '-Wall -O2 -march=native',
                          'fflags': '-Wall -Wno-maybe-uninitialized -Wno-surprising -O2 -march=native -fbacktrace',
                          'configure_flags_common': '--enable-mpi --with-elpa-prefix=$EBROOTELPA --common-flags --with-fftw-prefix=$EBROOTFFTW',
                          'configure_flags_mpi_common': '--enable-mpi --with-elpa-prefix=$EBROOTELPA --common-flags --with-fftw-prefix=$EBROOTFFTW --enable-mpi --with-elpa-prefix=$EBROOTELPA --with-pfft-prefix=$EBROOTPFFT',
                          'configure_flags_serial': '--enable-mpi --with-elpa-prefix=$EBROOTELPA --common-flags --with-fftw-prefix=$EBROOTFFTW --with-blas=""-L$BLAS_LIB_DIR $LIBBLAS""'
                          }
                 }

    # Flags with substitutions performed
    assert list(ref_flags) == ['pm', 'foss'], 'Top level keys, such as `pm`, do not get substituted'
    assert builder_config.set_flags(PackageManager.EB) == ref_flags


def test_builderconfig_get_builders(builder_config):

    builders_ref = {'foss':
                        {'2020b_mpi':
                             {'buildname': 'foss-2020b_mpi',
                              'toolchain': 'foss',
                              'version': '2020b_mpi',
                              'package_manager': PackageManager.EB,
                              'variant': '',
                              'mpi': False,
                              'cuda': False,
                              'test': 'full',
                              'timeout': 1800,
                              'testCommand': None,
                              'modulecmd': '/usr/share/lmod/lmod/libexec/lmod',
                              'codeCoverage': False,
                              'valgrind': False,
                              'failOnCompilerWarnings': False,
                              'workers': 'tentacles',
                              'var': {'CFLAGS': '-Wall -O2 -march=native',
                                      'CXXFLAGS': '-Wall -O2 -march=native',
                                      'FCFLAGS': '-Wall -Wno-maybe-uninitialized -Wno-surprising -O2 -march=native -fbacktrace',
                                      'LOCAL_MAKEFLAGS': '-j 16',
                                      'OCT_TEST_NJOBS': 16
                                      },
                              'flags': '--enable-mpi --with-elpa-prefix=$EBROOTELPA '
                                       '--common-flags --with-fftw-prefix=$EBROOTFFTW '
                                       '--with-blas=""-L$BLAS_LIB_DIR $LIBBLAS""'
                              }
                         }
                    }

    assert builder_config.get_builders(PackageManager.EB) == builders_ref


@pytest.fixture()
def builders_with_unexpected_toolchain_str() -> str:
    string = """
    environment_variables:
        buildbot_modulecmd: &buildbot_modulecmd '/usr/share/lmod/lmod/libexec/lmod'

    optional_builder_defaults:
       variant: ''
       mpi: False
       cuda: False
       test: 'full'
       timeout: 1800
       testCommand:
       modulecmd: *buildbot_modulecmd
       codeCoverage: False
       valgrind: False
       failOnCompilerWarnings: False

    # Builder flags
    # Order of definitions matters
    flags:
      foss:
        cflags:          '-Wall -O2 -march=native'
        cxxflags:        '-Wall -O2 -march=native'
        fflags:       '-Wall -Wno-maybe-uninitialized -Wno-surprising -O2 -march=native -fbacktrace'

    # Builder
    EB:
      foss:
        2020b_mpi:
          workers: 'tentacles'
          var:
            CFLAGS: '{foss.cflags}'
            CXXFLAGS: '{foss.cxxflags}'
            FCFLAGS: '{foss.fflags}'
            LOCAL_MAKEFLAGS: '-j 16'
            OCT_TEST_NJOBS: 16
          flags: 'another-flag'

    unexpected_field:
      tool-chain-version:
        workers: 'tentacles'
        var:
          CFLAGS: '{foss.cflags}'
          CXXFLAGS: '{foss.cxxflags}'
          FCFLAGS: '{foss.fflags}'
          LOCAL_MAKEFLAGS: '-j 16'
          OCT_TEST_NJOBS: 16
        flags: 'another-flag'
    """
    return string


@pytest.fixture()
def builders_yaml_with_unused_field(tmp_path, builders_with_unexpected_toolchain_str) -> MockFile:
    file = tmp_path / 'builders.yaml'
    file.write_text(builders_with_unexpected_toolchain_str)
    return MockFile(file, builders_with_unexpected_toolchain_str)


def test_builderconfig(builders_yaml_with_unused_field):

    with pytest.raises(ValueError) as error:
        b_config = BuilderConfig(builders_yaml_with_unused_field.file, perform_checks=True)
        assert b_config.file_name == builders_yaml_with_unused_field.full_path
        assert error.value.args[0] == f"Unused fields present in {b_config.file_name}: {{unexpected_field}}"
