import pytest
import yaml

from src.yaml_spec import YamlFormat, PackageManager
from src.parse_flags import process_flag, check_flag_substitutions, process_flags, get_lib_root_replacements, \
    substitute_lib_root_variable


@pytest.fixture()
def mock_explicit_flags() -> dict:
    yaml_string = """
    eb:
      configure_flags_min: '--with-libxc-prefix=$EBROOTLIBXC --with-gsl-prefix=$EBROOTGSL'
      configure_flags_omp: '--enable-openmp'
      configure_flags_blas: '--with-blas=""-L$BLAS_LIB_DIR $LIBBLAS""'
      """
    return yaml.safe_load(yaml_string)


@pytest.fixture()
def yfmt() -> YamlFormat:
    return YamlFormat()


def test_substitute_string_with_flag_example(yfmt, mock_explicit_flags):
    flag_string = '{eb.configure_flags_min} {eb.configure_flags_blas} --with-fftw-prefix=$EBROOTFFTW'
    processed_flags = process_flag(yfmt, mock_explicit_flags, flag_string)

    assert processed_flags == '--with-libxc-prefix=$EBROOTLIBXC --with-gsl-prefix=$EBROOTGSL ' \
                              '--with-blas=""-L$BLAS_LIB_DIR $LIBBLAS"" --with-fftw-prefix=$EBROOTFFTW'


def test_check_flag_substitutions():
    # One value contains a variable {}
    flags = {'eb':
                 {'configure_flags_omp': '--enable-openmp',
                  'configure_flags_min': '--with-libxc-prefix=$EBROOTLIBXC'},
             'foss':
                 {'cflags': '-Wall -O2 -march=native',
                  'configure_flags_common': '{eb.configure_flags_mpi}'}
             }

    missed_flags = check_flag_substitutions(flags)
    assert missed_flags == {'foss.configure_flags_common': '{eb.configure_flags_mpi}'}


def test_process_flags(yfmt):
    flags = {'eb':
                 {'configure_flags_min': '--some-min-flags',
                  'configure_flags_common': '--some-common-flags',
                  'configure_flags_omp': '--enable-openmp',
                  'configure_flags_blas': '--with-blas=""-L$BLAS_LIB_DIR $LIBBLAS""'},
             'foss': {'cflags_debug': '-g -Wall -O2 -march=native -Wextra -pedantic',
                      'cflags': '-Wall -O2 -march=native',
                      'cflags_opt': '-Wall -O3 -march=native -funroll-loops',
                      'configure_flags_min': '{eb.configure_flags_min} {eb.configure_flags_blas} --with-fftw-prefix=$EBROOTFFTW',
                      'configure_flags_min_mpi': '{foss.configure_flags_min}',
                      'configure_flags_common': '{eb.configure_flags_blas}'
                      }
             }

    ref_processed_flags = {'eb':
                               {'configure_flags_min': '--some-min-flags',
                                'configure_flags_common': '--some-common-flags',
                                'configure_flags_omp': '--enable-openmp',
                                'configure_flags_blas': '--with-blas=""-L$BLAS_LIB_DIR $LIBBLAS""'},
                           'foss':
                               {'cflags_debug': '-g -Wall -O2 -march=native -Wextra -pedantic',
                                'cflags': '-Wall -O2 -march=native',
                                'cflags_opt': '-Wall -O3 -march=native -funroll-loops',
                                'configure_flags_min': '--some-min-flags --with-blas=""-L$BLAS_LIB_DIR $LIBBLAS"" --with-fftw-prefix=$EBROOTFFTW',
                                'configure_flags_min_mpi': '--some-min-flags --with-blas=""-L$BLAS_LIB_DIR $LIBBLAS"" --with-fftw-prefix=$EBROOTFFTW',
                                'configure_flags_common': '--with-blas=""-L$BLAS_LIB_DIR $LIBBLAS""'}
                           }

    processed_flags = process_flags(yfmt, flags)

    # Expect all recursive_flags to have been substituted
    assert processed_flags['foss'][
               'configure_flags_min'] == '--some-min-flags --with-blas=""-L$BLAS_LIB_DIR $LIBBLAS"" --with-fftw-prefix=$EBROOTFFTW'
    assert processed_flags['foss'][
               'configure_flags_min_mpi'] == '--some-min-flags --with-blas=""-L$BLAS_LIB_DIR $LIBBLAS"" --with-fftw-prefix=$EBROOTFFTW'
    assert processed_flags['foss']['configure_flags_common'] == '--with-blas=""-L$BLAS_LIB_DIR $LIBBLAS""'

    assert processed_flags == ref_processed_flags, 'Check all flags are as expected'


def test_get_lib_root_replacements(yfmt):

    flags = {'pm':
                 {'configure_flags_min': '--with-libxc-prefix=$PMROOT_LIBXC --with-gsl-prefix=$PMROOT_GSL',
                  'configure_flags_common': '--with-netcdf-prefix=$PMROOT_NETCDFMINFORTRAN --with-etsf-io-prefix=$PMROOT_ETSF_IO'
                  },
             'foss':
                 {'cflags_debug': '-g -Wall -O2 -march=native -Wextra -pedantic'}
             }

    package_manager = PackageManager.EB
    replacements = get_lib_root_replacements(flags, yfmt, package_manager)

    ref_replacements = {'pm.configure_flags_min': '--with-libxc-prefix=$EBROOTLIBXC --with-gsl-prefix=$EBROOTGSL',
                        'pm.configure_flags_common': '--with-netcdf-prefix=$EBROOTNETCDFMINFORTRAN --with-etsf-io-prefix=$EBROOTETSF_IO'}

    assert replacements == ref_replacements


def test_substitute_lib_root_variable():
    flags = {'pm':
                 {'configure_flags_min': '--with-libxc-prefix=$PMROOT_LIBXC --with-gsl-prefix=$PMROOT_GSL',
                  'configure_flags_common': '--with-netcdf-prefix=$PMROOT_NETCDFMINFORTRAN --with-etsf-io-prefix=$PMROOT_ETSF_IO'},
             'foss': {'configure_flags_min': 'some-flag',
                      'configure_flags_common': 'another-flag'}
             }

    ref_output = {'pm': {'configure_flags_min': '--with-libxc-prefix=$EBROOTLIBXC --with-gsl-prefix=$EBROOTGSL',
                         'configure_flags_common': '--with-netcdf-prefix=$EBROOTNETCDFMINFORTRAN --with-etsf-io-prefix=$EBROOTETSF_IO'},
                  'foss': {'configure_flags_min': 'some-flag',
                           'configure_flags_common': 'another-flag'}
                  }

    package_manager = PackageManager.EB
    updated_flags = substitute_lib_root_variable(package_manager, flags)
    assert updated_flags == ref_output
